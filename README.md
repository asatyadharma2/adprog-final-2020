# EAI Course Student Project 2019

## CI/CD Variables

You need to prepare the following variables on your GitLab CI/CD project:

- `HEROKU_APP_NAME` - Name of the app on your Heroku account
- `HEROKU_API_KEY` - The API key required for deploying the app via your Heroku account

## Original Contributors

The original (`upstream`) repository can be found at:
https://gitlab.com/tengkuizdihar/adftugasakhir

- [Adrika Novrialdi](https://gitlab.com/adrika-novrialdi)
- [Farhan Nurdiatama Pakaya](https://gitlab.com/farhan.np9)
- [Tengku Izdihar](https://gitlab.com/tengkuizdihar)
=======
# adprog-final-2020